## Personal Website

My own portfolio website created with `HTML`, `CSS` and `JavaScript`.

## Technology Stack
- __ReactJS__ (Frontend)
- __ExpressJS__ (Backend)

## Features:
- __Home__ : Introduction and social links.
- __About__: A little biography.
- __Projects__: Overview of selected projects.
- __Resume__: Displaying my resume as a webpage.